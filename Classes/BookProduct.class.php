<?php
class BookProduct extends Item{
	protected $author;

	public function __construct($title, $price, $author){
		parent::__construct($title, $price);
		$this->author = $author;
	}

	public function getAuthor(){
		return $this->author;
	}
}